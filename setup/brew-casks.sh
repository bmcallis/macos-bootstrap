#!/usr/bin/env bash

for cask in `cat brew-casks.txt`; do
  if [[ ${cask:0:1} != '#' ]]; then
    if [[ `brew cask ls --versions $cask` ]]; then
      echo `brew cask ls --versions $cask` already installed
    else
      brew cask install $cask
    fi
  fi
done

