#!/usr/bin/env bash

for formula in `cat atom.packages.txt`; do
  if [[ ${formula:0:1} != '#' ]]; then
    if [[ `brew ls --versions $formula` ]]; then
      echo `brew ls --versions $formula` already installed
    else
      brew install $formula
    fi
  fi
done
