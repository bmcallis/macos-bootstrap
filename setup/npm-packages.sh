#!/usr/bin/env bash

if [[ `command -v npm` ]]; then
  for pkg in `cat npm-packages.txt`; do
    if [[ ${pkg:0:1} != '#' ]]; then
      if [[ `npm list -g --depth=0 | grep $pkg` ]]; then
        echo "npm package $pkg already installed"
      else
        npm install -g $pkg
      fi
    fi
  done
else
  echo "npm must be installed first"
fi
