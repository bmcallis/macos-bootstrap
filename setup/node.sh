#!/usr/bin/env bash

if [ `command -v brew` ]; then
  brew install nvm
  mkdir -p ~/.nvm
  export NVM_DIR="$HOME/.nvm"
  . "/usr/local/opt/nvm/nvm.sh"
  nvm install --lts
  nvm alias default node
  echo "Ensure that nvm is sourced upon login"
else
  echo "brew cask needs to be installed first"
  exit 1
fi

