#!/usr/bin/env bash

for formula in `cat brew-formulae.txt`; do
  if [[ ${formula:0:1} != '#' ]]; then
    if [[ `brew ls --versions $formula` ]]; then
      echo `brew ls --versions $formula` already installed
    else
      brew install $formula
    fi
  fi
done

if [[ `brew ls --versions icu4c` ]]; then
  # needed for some npm packges, more info here: https://github.com/mooz/node-icu-charset-detector#osx
  brew link icu4c --force
fi

