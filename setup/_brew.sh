#/usr/bin/env bash

if [ `command -v brew` ]; then
  echo "brew already installed"
  brew --version
else
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi


